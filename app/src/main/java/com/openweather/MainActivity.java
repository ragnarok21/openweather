package com.openweather;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.openweather.db.DataBaseManager;
import com.openweather.listAdapter.MyListViewAdapter;
import com.openweather.model.City;
import com.openweather.model.GeoCode;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private DataBaseManager mManager;
    private ListView listView;
    final Context context = this;
    private  MyListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Button backto_main = (Button) findViewById(R.id.button);
        backto_main.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, AddCity.class);
                //myIntent.putExtra("key", value); //Optional parameters
                MainActivity.this.startActivity(myIntent);
            }
        });

        listView = (ListView)findViewById(R.id.listViewWeather);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this,DetailCity.class);
                Bundle bundle = new Bundle();
                try {
                    bundle.putSerializable("geocode",getCityList().get(i).getGeoCode());
                    intent.putExtras(bundle);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            }
        });
    listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        CheckBox checkbox = (CheckBox) listView.getChildAt(i).findViewById(R.id.checkBox);
        checkbox.setVisibility(View.VISIBLE);
        checkbox.setChecked(true);
        findViewById(R.id.buttonDelete).setVisibility(View.VISIBLE);
        findViewById(R.id.button).setVisibility(View.GONE);
        return true;

    }
});

    }

    private void init() throws IOException {
        mManager = new DataBaseManager(this);
        if(getCityList().size()>0){
            findViewById(R.id.notFoundTxt).setVisibility(View.GONE);
            getListDataResponse();
        }else{
           findViewById(R.id.listViewWeather).setVisibility(View.GONE);
        }

    }
    private void insertDataTest() throws JsonProcessingException {
        City city = new City();
        //SANTIAGO OF CHILE
        city.setName("Santiago de Chile");
        city.setZipCode("8320000");
        city.setGeoCode(new GeoCode(-33.45,-70.66));
        mManager.createCity(city);

        //MILAN-ITALY
        city.setName("Milan");
        city.setZipCode("20100");
        city.setGeoCode(new GeoCode(45.46,9.18));
        mManager.createCity(city);
    }

    private City getCity(int id){
        City city = mManager.getCity(id);
        return city;
    }
    private City getCityByName(String cityName){
        City city = mManager.getCityByName(cityName);
        return city;
    }

    private List<City> getCityList() throws IOException {
        return mManager.getCities();
    }

    private void getListDataResponse() throws IOException {
        ListView listview = (ListView)findViewById(R.id.listViewWeather);
        adapter = new MyListViewAdapter(this);

        listview.setAdapter(adapter);
        adapter.setData(getCityList());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonDelete:
                /*Add delete implementation*/

                AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle("Delete elements")
                .setMessage("Are you sure to delete this elements?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                for (int i = 0; i < listView.getChildCount(); i++) {
                                    CheckBox c = (CheckBox)listView.getChildAt(i).findViewById(R.id.checkBox);
                                    if(c.isChecked()){
                                        TextView name = (TextView)listView.getChildAt(i).findViewById(R.id.cityNameValue);
                                        mManager.deleteCityByName(name.getText().toString());
                                        adapter.remove(i);
                                        adapter.notifyDataSetChanged();
                                    }
                                }


                                for (int i = 0; i <listView.getChildCount() ; i++) {
                                    listView.getChildAt(i).findViewById(R.id.checkBox).setVisibility(View.GONE);
                                }
                                findViewById(R.id.buttonDelete).setVisibility(View.GONE);
                                findViewById(R.id.button).setVisibility(View.VISIBLE);
                                Toast.makeText(context,"Elements deleted",Toast.LENGTH_LONG).show();
                                dialogInterface.cancel();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int j) {
                                for (int i = 0; i <listView.getChildCount() ; i++) {
                                    listView.getChildAt(i).findViewById(R.id.checkBox).setVisibility(View.GONE);
                                }
                                findViewById(R.id.buttonDelete).setVisibility(View.GONE);
                                findViewById(R.id.button).setVisibility(View.VISIBLE);
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }
}
