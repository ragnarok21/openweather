package com.openweather.listAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.openweather.R;
import com.openweather.model.City;

import java.util.ArrayList;
import java.util.List;


public class MyListViewAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<City> mDataList = new ArrayList<>();

    public MyListViewAdapter(Context cxt){
        mInflater = LayoutInflater.from(cxt);
    }

    public void setData(List<City> dataList){
        this.mDataList = dataList;
    }

    public void remove(int i){
        mDataList.remove(i);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        City data = mDataList.get(position);

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.itemweather,null);
            System.out.println("item created for "+position);
        }

        ((TextView)convertView.findViewById(R.id.cityNameValue)).setText(data.getName());
        ((TextView)convertView.findViewById(R.id.zipCodeValue)).setText(data.getZipCode());
        ((TextView)convertView.findViewById(R.id.geoCodeValue)).setText("["+data.getGeoCode().getLatitude()+", "+data.geoCode.getLongitude()+"]");

        return convertView;

    }
}
