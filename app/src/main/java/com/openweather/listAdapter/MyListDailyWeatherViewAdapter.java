package com.openweather.listAdapter;

import android.content.Context;
import android.icu.text.DecimalFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.openweather.R;
import com.openweather.model.DayWeather;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class MyListDailyWeatherViewAdapter extends BaseAdapter {

    //Summary Types
    public static final String CLEAR_DAY = "clear-day";
    public static final String CLEAR_NIGHT = "clear-night";
    public static final String RAIN = "rain";
    public static final String SNOW = "snow";
    public static final String SLEET = "sleet";
    public static final String WIND = "wind";
    public static final String FOG = "fog";
    public static final String CLOUDY = "cloudy";
    public static final String PARTLY_CLOUD_DAY = "partly-cloudy-day";
    public static final String PARTLY_CLOUD_NIGHT = "partly-cloudy-night";

    private LayoutInflater mInflater;
    private List<DayWeather> mDataList = new ArrayList<>();

    public MyListDailyWeatherViewAdapter(Context cxt){
        mInflater = LayoutInflater.from(cxt);
    }

    public void setData(List<DayWeather> dataList){
        this.mDataList = dataList;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DayWeather data = mDataList.get(position);

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.dailyitemweather,null);
            System.out.println("item created for "+position);
        }

        ((TextView)convertView.findViewById(R.id.dateValue)).setText(unixTimeToDate(data.getTime()));
        ((TextView)convertView.findViewById(R.id.maxValue)).setText(fahrenheitToCelsius(data.getTemperatureMax())+" °C");
        ((TextView)convertView.findViewById(R.id.humidityValue)).setText(String.valueOf(data.getHumidity()));
        ((TextView)convertView.findViewById(R.id.minValue)).setText(fahrenheitToCelsius(data.getTemperatureMin())+" °C");
        ((TextView)convertView.findViewById(R.id.windValue)).setText(String.valueOf(data.getWindSpeed()));


        switch (data.getIcon()){

            case CLEAR_DAY:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.clear_day);
                break;

            case CLEAR_NIGHT:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.clear_night);
                break;

            case RAIN:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.rain);
                break;

            case SNOW:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.snow);
                break;

            case SLEET:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.sleet);
                break;

            case WIND:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.wind);
                break;

            case FOG:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.fog);
                break;

            case CLOUDY:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.cloudy);
                break;

            case PARTLY_CLOUD_DAY:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.partlycloudyday);
                break;

            case PARTLY_CLOUD_NIGHT:
                ((ImageView)convertView.findViewById(R.id.summaryType)).setImageResource(R.drawable.partlycloudynight);
                break;
        }

        return convertView;

    }

    private String fahrenheitToCelsius(double fahrenheitValue){
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);
        return df.format(((fahrenheitValue - 32)*5)/9);
    }

    private String unixTimeToHours(int unixTime){
        Date date = new Date(unixTime*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));;
        return sdf.format(date);
    }

    private String unixTimeToDate(int unixTime){
        Date date = new Date(unixTime*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        return sdf.format(date);
    }
}
