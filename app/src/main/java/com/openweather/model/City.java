package com.openweather.model;

import java.io.Serializable;

public class City implements Serializable{

    private static final long serialVersionUID = -7060210544600464481L;

    public int id;
    public String name;
    public String zipCode;
    public GeoCode geoCode;

    public City(){}

    public City(String n,String z,GeoCode g){
        this.name = n;
        this.zipCode = z;
        this.geoCode = g;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public GeoCode getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(GeoCode geoCode) {
        this.geoCode = geoCode;
    }
}
