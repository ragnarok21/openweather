package com.openweather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CityWeatherResponse {

    private String summary;
    private List<DayWeather> data;

    public CityWeatherResponse(){}

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<DayWeather> getData() {
        return data;
    }

    public void setData(List<DayWeather> data) {
        this.data = data;
    }
}
