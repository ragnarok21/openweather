package com.openweather.model;

import java.io.Serializable;

public class GeoCode implements Serializable{

    private static final long serialVersionUID = -7060210544600464481L;

    private double latitude;
    private double longitude;

    public GeoCode(){}

    public GeoCode(double lat , double lon){
        this.latitude = lat;
        this.longitude = lon;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
