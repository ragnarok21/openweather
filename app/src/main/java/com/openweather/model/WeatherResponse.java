package com.openweather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

    private double latitude;
    private double longitude;
    private CityWeatherResponse daily;

    public WeatherResponse(){}

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public CityWeatherResponse getDaily() {
        return daily;
    }

    public void setDaily(CityWeatherResponse daily) {
        this.daily = daily;
    }
}
