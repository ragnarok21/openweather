package com.openweather.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.openweather.model.City;
import com.openweather.model.GeoCode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataBaseManager extends SQLiteOpenHelper {

    public static final String dbName ="mydb";


    public DataBaseManager(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table city " +
                        "(id integer primary key, name text,zipcode text,geocodes text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS city");
        onCreate(db);
    }

    public boolean createCity (City c) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String jsonString = ow.writeValueAsString(c.getGeoCode());
        contentValues.put("name", c.name);
        contentValues.put("zipcode", c.zipCode);
        contentValues.put("geocodes", jsonString);
        db.insert("city", null, contentValues);
//        db.execSQL("");
        return true;
    }


    public City getCity(int id){
        ObjectMapper ow = new ObjectMapper();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from city where id="+id+"", null );

        City c = new City();

        try{
            cursor.moveToFirst();
            c.id = id;
            c.name = cursor.getString(cursor.getColumnIndex("name"));
            c.zipCode =cursor.getString(cursor.getColumnIndex("zipcode"));
            GeoCode geoCode = ow.readValue(cursor.getString(cursor.getColumnIndex("geocodes")),GeoCode.class);
            c.geoCode = geoCode;
        }catch (Exception e){

        }
        return c;
    }

    public City getCityByName(String cityName){
        ObjectMapper ow = new ObjectMapper();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from city where name='"+cityName+"'", null );

        City c = new City();

        try{
            cursor.moveToFirst();
            c.id = Integer.parseInt(cursor.getString(cursor.getColumnIndex("id")));
            c.name = cursor.getString(cursor.getColumnIndex("name"));
            c.zipCode =cursor.getString(cursor.getColumnIndex("zipcode"));
            GeoCode geoCode = ow.readValue(cursor.getString(cursor.getColumnIndex("geocodes")),GeoCode.class);
            c.geoCode = geoCode;
        }catch (Exception e){

        }
        return c;
    }

    public List<City> getCities() throws IOException {
        List<City> cityList = new ArrayList<City>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from city", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            City city = new City();
            ObjectMapper ow = new ObjectMapper();
            city.setId(Integer.parseInt(res.getString(res.getColumnIndex("id"))));
            city.setName(res.getString(res.getColumnIndex("name")));
            city.setZipCode(res.getString(res.getColumnIndex("zipcode")));
            GeoCode geoCode = ow.readValue(res.getString(res.getColumnIndex("geocodes")),GeoCode.class);
            city.setGeoCode(geoCode);
            cityList.add(city);
            res.moveToNext();
        }
        return cityList;
    }

    public void deleteCityByName(String cityName){
        ObjectMapper ow = new ObjectMapper();
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete("city","name='"+cityName+"'",null);
    }

}
