package com.openweather;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.android.gms.maps.model.LatLng;
import com.openweather.db.DataBaseManager;
import com.openweather.model.City;
import com.openweather.model.GeoCode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddCity extends AppCompatActivity implements View.OnClickListener {

    private DataBaseManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        final Button map = (Button) findViewById(R.id.mapsBtn);
        final Button geocode = (Button) findViewById(R.id.geoCodesBtn);
        mManager = new DataBaseManager(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mapsBtn:
                Intent myIntent = new Intent(AddCity.this, Map.class);
                //myIntent.putExtra("key", value); //Optional parameters
                AddCity.this.startActivity(myIntent);
                break;

            case R.id.saveBtn:
                EditText cityName = (EditText) findViewById(R.id.cityNameEdit);
                if(!cityName.getText().toString().isEmpty()){
                    try {
                        String location = cityName.getText().toString();
                        Geocoder gc = new Geocoder(this);
                        List<Address> address = gc.getFromLocationName(location, 1);
                        Address oneLocation= address.get(0);




                                City city = new City();
                                city.setName(oneLocation.getFeatureName());
                                city.setZipCode(oneLocation.getPostalCode());
                                city.setGeoCode(new GeoCode(oneLocation.getLatitude(),oneLocation.getLongitude()));
                                try {
                                    mManager.createCity(city);
                                    Intent intent = new Intent(AddCity.this,MainActivity.class);
                                    startActivity(intent);
                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                }


                    } catch (IOException e) {
                        // handle the exception
                    }
                }
                break;
            case R.id.cancelBtn:
                Intent intent = new Intent(AddCity.this,MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}