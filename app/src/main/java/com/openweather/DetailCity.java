package com.openweather;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.openweather.listAdapter.MyListDailyWeatherViewAdapter;
import com.openweather.model.GeoCode;
import com.openweather.model.WeatherResponse;
import com.openweather.utils.Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class DetailCity extends AppCompatActivity {

    private final static String APY_KEY = "635772b308934c5c71166f24183a8385";
    private final static String ENDPOINT = "https://api.forecast.io/forecast";
    private GeoCode geoCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_city);
        Intent intent = getIntent();
        geoCode = (GeoCode)intent.getSerializableExtra("geocode");
        doInBackground();
        findViewById(R.id.addCityDetail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(DetailCity.this,AddCity.class);
                startActivity(intent1);
            }
        });
    }

    private void doInBackground(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                backToMainThreadWithResponse(fetchCityWeatherReport());
            }
        });

        thread.start();
    }

    private void backToMainThreadWithResponse(final WeatherResponse response) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                getListDataResponse(response);
            }
        });
    }

    private void getListDataResponse(WeatherResponse response) {
        ListView listview = (ListView)findViewById(R.id.listDailyWeather);
        MyListDailyWeatherViewAdapter adapter = new MyListDailyWeatherViewAdapter(this);

        listview.setAdapter(adapter);
        adapter.setData(response.getDaily().getData());
    }

    private WeatherResponse fetchCityWeatherReport(){
        try{
            URL url = new URL(ENDPOINT+"/"+APY_KEY+"/"+geoCode.getLatitude()+","+geoCode.getLongitude());
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection connection = null;
            if (urlConnection instanceof HttpURLConnection) {
                connection = (HttpURLConnection) urlConnection;
            } else {
                System.out.println("Please enter an HTTP URL.");
                return null;
            }
            String urlString = "";
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            String current;
            while ((current = in.readLine()) != null) {
                urlString += current;
            }

            //convert string to json using jackson
            System.out.println(urlString);
            return (WeatherResponse) Utils.fromJson(urlString,WeatherResponse.class);

        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
